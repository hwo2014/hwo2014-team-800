
var send, myCar, gameInit, gameEnd, crash, spawn, laps = [],
    dnf, fin, positions = [], isStarted, tick = 0;


/* ----- common ----- */
function log(){
  console.log.apply(this, arguments)
}
exports.log = log;

function dump(data){
  log(JSON.stringify(data));
}
exports.dump = dump;

function isNumber(n){
  return ! isNaN(n);
}

/* ----- piece calculation ----- */
function getPieceByCarPositionData(d){
  return gameInit.data.race.track.pieces[d.piecePosition.pieceIndex];
}

function getLaneByCarPositionData(d){
  return gameInit.data.race.track.lanes[d.piecePosition.lane.startLaneIndex];
}

function getRadiusByCarPositionData(d){
  if (! d || ! d.piecePosition || ! d.piecePosition.pieceIndex){
    return 0;
  }
  var piece = getPieceByCarPositionData(d);
  if (! piece.angle){
    return 0;
  }
  var lane = getLaneByCarPositionData(d);
  var distanceFromCenter = lane.distanceFromCenter;
  if (0 < piece.angle){
    distanceFromCenter = -1 * distanceFromCenter;
  }
  return piece.radius + distanceFromCenter;
}

function getCornerLengthByCarPositionData(d){
  return  getRadiusByCarPositionData(d) * 2 * Math.PI * getPieceByCarPositionData(d).angle / 360;
}


/* ----- race start ----- */
exports.init = function(sender){
  send = sender;
}

exports.gameInit = function(data){
  gameInit = data;
}

exports.gameStart = function(data){
  isStarted = true;
  startTime = new Date().getTime();
  var msg = 'tick,piceIndex,angle,distanceDiff,inPieceDistance,endLaneIndex';
  log(msg);
}

exports.yourCar = function(data){
  myCar = data;
}


/* ----- race end ----- */
exports.gameEnd = function(data){
  gameEnd = data;
}

exports.finish = function(data){
  fin = data;
}

exports.tournamentEnd = function(data){

}

exports.dnf = function(data){
  dnf = data;
}

exports.error = function(){

}


/* ----- on racing ----- */
exports.crash = function(data){
  log('crash')
  crash = data;
}

exports.spawn = function(data){
  spawn = data;
}

exports.onLapFinished = function(data){
  laps.push(data);
}

exports.carPositions = function(data){
  var skip = data.gameTick - tick;
  if (1 < skip){
    console.log('tick skip ' + skip) //no need ??
  }
  tick = data.gameTick;

  var velocity = calcVelocity(data);
  positions.push(data);
  var d = data.data[0];
  d.v = velocity;
  var msg = tick + ',' + d.piecePosition.pieceIndex + ',' + d.angle + ',' + d.v + ',' + d.piecePosition.inPieceDistance + ',' + d.piecePosition.lane.endLaneIndex;
  //log(msg);
  send({
    msgType: "throttle",
    data: 1
  });
}

function calcVelocity(data){
  if (! isStarted) return 0;
  var d = data.data[0];

  var prevTick = 0,
      prevPos = 0,
      distance = 0,
      nowPos = d.piecePosition.inPieceDistance;

  if (0 < positions.length){
    var prev = positions[positions.length - 1],
        pd = prev.data[0];
    prevTick = prev.gameTick;
    prevPos = pd.piecePosition.inPieceDistance;

    if (! prev){
      distance = nowPos;
    }else if (d.piecePosition.pieceIndex == pd.piecePosition.pieceIndex){
      distance = nowPos - prevPos;
    }else{
      var piece = getPieceByCarPositionData(pd);
      var length = piece.length;
      if (piece.radius){
        length = getCornerLengthByCarPositionData(pd);
      }
      distance = length - pd.piecePosition.inPieceDistance + nowPos;
    }
  };
  return distance;
}
