
var controller = require('./HwoController');

function send(json) {
//  console.log('send data as [' + JSON.stringify(json) + ']')
};

function dummy(data){
  var fn = controller[data.msgType];
  if (fn){
    fn(data)
  }else{
    controller.log('ignored msgType [' + data.msgType + ']');
  }
}

var gameInit = {"msgType":"gameInit","data":{"race":{"track":{"id":"keimola","name":"Keimola","pieces":[{"length":100},{"length":100},{"length":100},{"length":100,"switch":true},{"radius":100,"angle":45},{"radius":100,"angle":45},{"radius":100,"angle":45},{"radius":100,"angle":45},{"radius":200,"angle":22.5,"switch":true},{"length":100},{"length":100},{"radius":200,"angle":-22.5},{"length":100},{"length":100,"switch":true},{"radius":100,"angle":-45},{"radius":100,"angle":-45},{"radius":100,"angle":-45},{"radius":100,"angle":-45},{"length":100,"switch":true},{"radius":100,"angle":45},{"radius":100,"angle":45},{"radius":100,"angle":45},{"radius":100,"angle":45},{"radius":200,"angle":22.5},{"radius":200,"angle":-22.5},{"length":100,"switch":true},{"radius":100,"angle":45},{"radius":100,"angle":45},{"length":62},{"radius":100,"angle":-45,"switch":true},{"radius":100,"angle":-45},{"radius":100,"angle":45},{"radius":100,"angle":45},{"radius":100,"angle":45},{"radius":100,"angle":45},{"length":100,"switch":true},{"length":100},{"length":100},{"length":100},{"length":90}],"lanes":[{"distanceFromCenter":-10,"index":0},{"distanceFromCenter":10,"index":1}],"startingPoint":{"position":{"x":-300,"y":-44},"angle":90}},"cars":[{"id":{"name":"Green Coder","color":"red"},"dimensions":{"length":40,"width":20,"guideFlagPosition":10}}],"raceSession":{"laps":3,"maxLapTimeMs":60000,"quickRace":true}}},"gameId":"1baae020-e8a4-43b4-9582-0356fcf4a89a"};
var gameStart = {"msgType": "gameStart", "data": null};
var finish = {"msgType":"finish","data":{"name":"Green Coder","color":"red"},"gameId":"29e69dfa-938b-4ddd-b0f9-a4be1d766c5b"};

controller.init(send);
dummy(gameInit);
dummy(gameStart);

dummy({"msgType":"carPositions","data":[{"id":{"name":"Green Coder","color":"red"},"angle":0,"piecePosition":{"pieceIndex":0,"inPieceDistance":0,"lane":{"startLaneIndex":0,"endLaneIndex":0},"lap":0}}],"gameId":"b974fb3f-8a4b-4e9a-b0e0-88a4216419ea","gameTick":1});
dummy({"msgType":"carPositions","data":[{"id":{"name":"Green Coder","color":"red"},"angle":0,"piecePosition":{"pieceIndex":0,"inPieceDistance":0.2,"lane":{"startLaneIndex":0,"endLaneIndex":0},"lap":0}}],"gameId":"b974fb3f-8a4b-4e9a-b0e0-88a4216419ea","gameTick":2});
dummy({"msgType":"carPositions","data":[{"id":{"name":"Green Coder","color":"red"},"angle":0,"piecePosition":{"pieceIndex":0,"inPieceDistance":0.25,"lane":{"startLaneIndex":0,"endLaneIndex":0},"lap":0}}],"gameId":"b974fb3f-8a4b-4e9a-b0e0-88a4216419ea","gameTick":3});
dummy({"msgType":"carPositions","data":[{"id":{"name":"Green Coder","color":"red"},"angle":0,"piecePosition":{"pieceIndex":0,"inPieceDistance":42,"lane":{"startLaneIndex":0,"endLaneIndex":0},"lap":0}}],"gameId":"b974fb3f-8a4b-4e9a-b0e0-88a4216419ea","gameTick":4});
dummy({"msgType":"carPositions","data":[{"id":{"name":"Green Coder","color":"red"},"angle":0,"piecePosition":{"pieceIndex":0,"inPieceDistance":91.1,"lane":{"startLaneIndex":0,"endLaneIndex":0},"lap":0}}],"gameId":"b974fb3f-8a4b-4e9a-b0e0-88a4216419ea","gameTick":2});

dummy({"msgType":"carPositions","data":[{"id":{"name":"Green Coder","color":"red"},"angle":0,"piecePosition":{"pieceIndex":1,"inPieceDistance":0,"lane":{"startLaneIndex":0,"endLaneIndex":0},"lap":0}}],"gameId":"b974fb3f-8a4b-4e9a-b0e0-88a4216419ea","gameTick":1});
dummy({"msgType":"carPositions","data":[{"id":{"name":"Green Coder","color":"red"},"angle":0,"piecePosition":{"pieceIndex":1,"inPieceDistance":92.2,"lane":{"startLaneIndex":0,"endLaneIndex":0},"lap":0}}],"gameId":"b974fb3f-8a4b-4e9a-b0e0-88a4216419ea","gameTick":2});

dummy({"msgType":"carPositions","data":[{"id":{"name":"Green Coder","color":"red"},"angle":0,"piecePosition":{"pieceIndex":2,"inPieceDistance":0,"lane":{"startLaneIndex":0,"endLaneIndex":0},"lap":0}}],"gameId":"b974fb3f-8a4b-4e9a-b0e0-88a4216419ea","gameTick":1});
dummy({"msgType":"carPositions","data":[{"id":{"name":"Green Coder","color":"red"},"angle":0,"piecePosition":{"pieceIndex":2,"inPieceDistance":93.3,"lane":{"startLaneIndex":0,"endLaneIndex":0},"lap":0}}],"gameId":"b974fb3f-8a4b-4e9a-b0e0-88a4216419ea","gameTick":2});

dummy({"msgType":"carPositions","data":[{"id":{"name":"Green Coder","color":"red"},"angle":0,"piecePosition":{"pieceIndex":3,"inPieceDistance":0,"lane":{"startLaneIndex":0,"endLaneIndex":0},"lap":0}}],"gameId":"b974fb3f-8a4b-4e9a-b0e0-88a4216419ea","gameTick":1});
dummy({"msgType":"carPositions","data":[{"id":{"name":"Green Coder","color":"red"},"angle":0,"piecePosition":{"pieceIndex":3,"inPieceDistance":94.4,"lane":{"startLaneIndex":0,"endLaneIndex":0},"lap":0}}],"gameId":"b974fb3f-8a4b-4e9a-b0e0-88a4216419ea","gameTick":2});

dummy({"msgType":"carPositions","data":[{"id":{"name":"Green Coder","color":"red"},"angle":0,"piecePosition":{"pieceIndex":4,"inPieceDistance":0,"lane":{"startLaneIndex":0,"endLaneIndex":0},"lap":0}}],"gameId":"b974fb3f-8a4b-4e9a-b0e0-88a4216419ea","gameTick":1});
dummy({"msgType":"carPositions","data":[{"id":{"name":"Green Coder","color":"red"},"angle":0,"piecePosition":{"pieceIndex":4,"inPieceDistance":57,"lane":{"startLaneIndex":0,"endLaneIndex":0},"lap":0}}],"gameId":"b974fb3f-8a4b-4e9a-b0e0-88a4216419ea","gameTick":2});

dummy({"msgType":"carPositions","data":[{"id":{"name":"Green Coder","color":"red"},"angle":0,"piecePosition":{"pieceIndex":5,"inPieceDistance":0,"lane":{"startLaneIndex":0,"endLaneIndex":0},"lap":0}}],"gameId":"b974fb3f-8a4b-4e9a-b0e0-88a4216419ea","gameTick":1});
dummy({"msgType":"carPositions","data":[{"id":{"name":"Green Coder","color":"red"},"angle":0,"piecePosition":{"pieceIndex":5,"inPieceDistance":96.6,"lane":{"startLaneIndex":0,"endLaneIndex":0},"lap":0}}],"gameId":"b974fb3f-8a4b-4e9a-b0e0-88a4216419ea","gameTick":2});

dummy(finish);
