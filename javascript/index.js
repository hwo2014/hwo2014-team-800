var net = require("net");
var JSONStream = require('JSONStream');
var controller = require('./HwoController');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
  return send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });
});

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};

controller.init(send);

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) {
controller.dump(data);
  if (data.msgType === 'carPositions') {
    controller.carPositions(data);
  } else {
    var fn = controller[data.msgType];
    if (fn){
      fn(data)
    }else{
      controller.log('ignored msgType [' + data.msgType + ']');
    }
    send({
      msgType: "ping",
      data: {}
    });
  }
});

jsonStream.on('error', function() {
  console.log("disconnected");
  return controller.error();
});
