package noobbot.entity.dao;

import java.util.Date;

public class DaoLog {
	public String gameId;
	public String carName;
	public int gameTick;
	public int lap;
	public int pieceIndex;
	public double inPieceDistance;
	public int startLaneIndex;
	public int endLaneIndex;
	public double throttle;
	public Double velocity;
	public Double acceleration;
	public Boolean isCrash;
	public Date update_time;
}
