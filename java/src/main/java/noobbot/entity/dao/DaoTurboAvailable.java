package noobbot.entity.dao;

import java.util.Date;

public class DaoTurboAvailable {
	public String gameId;
	public double turboDurationMilliseconds;
	public int turboDurationTicks;
	public double turboFactor;
	public int gameTick;
	public Date updateTime;
}
