package noobbot.entity.dao;

import java.util.Date;

public class DaoPiece {
	public String gameId;
	public int pieceIndex;
	public Double length;
	public Double angle;
	public Double radius;
	public Boolean hasSwitch;
	public Date updateTime;
}
