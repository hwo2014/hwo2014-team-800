package noobbot.entity.msg.receive;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class GameInit {

	public Race race;
	
	public static class Race {

		public Track track;
		public List<Car> cars;
		public RaceSession raceSession;
		
		public static class Track{
			public String id;
			public String name;
			public List<Piece> pieces;
			public List<Lane> lanes;
			public StartingPoint startingPoint;
			
			public static class Piece{
				public Double length;
				public Double radius;
				public Double angle;
				@SerializedName("switch")
				public Boolean hasSwitch;
				public boolean isCorner(){
					return radius != null;
				}
			}
			
			public static class Lane{
				public double distanceFromCenter;
				public int index;
			}
			
			public static class StartingPoint{
				public double angle;
				public Position position;
				
				public static class Position{
					public int x;
					public int y;
				}
			}
		}
		
		public static class Car{
			public CarId id;
			public Dimensions dimensions;
			
			public static class Dimensions{
				public int guideFlagPosition;
				public int length;
				public int width;
			}
		}
		
		public static class RaceSession{
			public int laps;
			public int maxLapTimeMs;
			public boolean quickRace;
		}
	}
}
