package noobbot.entity.msg.receive;

public class ReceiveMsg<T>{
	public String msgType;
	public T data;
	public String gameId;
	public int gameTick;
}
