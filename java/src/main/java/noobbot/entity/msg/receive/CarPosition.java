package noobbot.entity.msg.receive;

public class CarPosition {
	public CarId id;
	public double angle;
	public PiecePosition piecePosition;
	
	public static class PiecePosition{
		public int pieceIndex;
		public double inPieceDistance;
		public Lane lane;
		public int lap;
	}
	
	public static class Lane{
		public int startLaneIndex;
		public int endLaneIndex;
	}
}
