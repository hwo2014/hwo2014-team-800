package noobbot.entity.msg.send;

import com.google.gson.Gson;

public abstract class SendMsg {

	public String toJson() {
        return new Gson().toJson(new Json(msgType(), msgData()));
    }
	
	protected Object msgData(){
    	return this;
    }

    protected abstract String msgType();
    
	private static class Json{
		@SuppressWarnings("unused")
		public String msgType;
		@SuppressWarnings("unused")
		public Object data;
		public Json(String msgType, Object data){
			this.msgType = msgType;
			this.data = data;
		}
	}
}
