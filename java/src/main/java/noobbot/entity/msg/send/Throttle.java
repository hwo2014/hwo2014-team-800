package noobbot.entity.msg.send;

public class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    public Double msgData() {
        return value;
    }

    @Override
    public String msgType() {
        return "throttle";
    }
}
