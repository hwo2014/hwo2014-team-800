package noobbot;

import java.io.IOException;

import noobbot.connector.Connector;
import noobbot.connector.RealConnector;
import noobbot.controller.Controller;
import noobbot.strategy.ConstantThrottleStrategy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
	
	static final Logger logger = LoggerFactory.getLogger(Main.class);
	
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        logger.info("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
        
        Connector connector = new RealConnector(host, port, botName, botKey);
        new Controller(connector, new ConstantThrottleStrategy(0.60));
    }
}
