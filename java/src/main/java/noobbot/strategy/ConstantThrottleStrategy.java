package noobbot.strategy;

import java.util.Map;

import noobbot.entity.Operation;
import noobbot.entity.msg.receive.CarId;
import noobbot.entity.msg.receive.CarPosition;

public class ConstantThrottleStrategy extends Strategy {

	private double value;
	private boolean hasCrash;
	
	public ConstantThrottleStrategy(double value){
		this.value = value;
	}
	
	@Override
	public Operation calcThrottle(Map<String, CarPosition> carPositionMap, int tick) {
		Operation result = new Operation();
		result.throttle = value;
		if (hasCrash){
			result.throttle = null;
			result.isGiveUp = hasCrash;
		}
		return result;
	}
	
	@Override
	public void crash(CarId crashedCar, int tick){
		hasCrash = true;
	}
}
