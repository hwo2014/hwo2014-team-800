package noobbot.strategy;

import java.util.Map;

import noobbot.common.Constant;
import noobbot.entity.Operation;
import noobbot.entity.msg.receive.CarId;
import noobbot.entity.msg.receive.CarPosition;

public class DecelerationTestStrategy extends Strategy {

	private double value;
	private boolean hasCrash;
	
	public DecelerationTestStrategy(double value){
		this.value = value;
	}
	
	@Override
	public Operation calcThrottle(Map<String, CarPosition> carPositionMap, int tick) {
		Operation result = new Operation();
		int pieceIndex = carPositionMap.get(Constant.botName).piecePosition.pieceIndex;
		if (pieceIndex < 3){
			result.throttle = 1d;
			return result;
		}
		result.throttle = value;
		if (hasCrash || 6 < pieceIndex){
			result.throttle = null;
			result.isGiveUp = true;
		}
		return result;
	}
	
	@Override
	public void crash(CarId crashedCar, int tick){
		hasCrash = true;
	}
}
