package noobbot.strategy;

import java.util.Map;

import noobbot.entity.Operation;
import noobbot.entity.msg.receive.CarId;
import noobbot.entity.msg.receive.CarPosition;

public abstract class Strategy {

	public abstract Operation calcThrottle(Map<String, CarPosition> carPositionMap, int tick);
	
	public void crash(CarId carId, int tick){
	}
	
	public void spawn(CarId carId, int tick){
	}
}
