package noobbot.probe.accessor;

import java.sql.Connection;
import java.sql.PreparedStatement;

import noobbot.entity.dao.DaoLog;
import noobbot.probe.accessor.BaseAccessor.DoInConnection;

public class UpdateVelocity extends BaseAccessor implements DoInConnection{

	private DaoLog log;
	
	public UpdateVelocity(DaoLog log){
		this.log = log;
	}
	
	public int exec(){
		return (Integer) doSql(this);
	}
	
	@Override
	public Integer action(Connection conn) throws Exception{
		String sql = "update log set velocity = ?, acceleration = ?, isCrash = ? " +
						"where gameTick = ? and gameId = ? and carName = ?";
		PreparedStatement stat = conn.prepareStatement(sql);
		int i = 1;
		
		stat.setDouble(i++, log.velocity);
		stat.setDouble(i++, log.acceleration);
		stat.setBoolean(i++, log.isCrash);
		stat.setInt(i++, log.gameTick);
		stat.setString(i++, log.gameId);
		stat.setString(i++, log.carName);
		
		return stat.executeUpdate();
	}
}
