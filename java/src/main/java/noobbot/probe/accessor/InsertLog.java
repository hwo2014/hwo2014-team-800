package noobbot.probe.accessor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Date;
import java.util.List;

import noobbot.common.JsonParseUtil;
import noobbot.entity.msg.receive.CarPosition;
import noobbot.entity.msg.receive.ReceiveMsg;
import noobbot.probe.accessor.BaseAccessor.DoInConnection;

public class InsertLog extends BaseAccessor implements DoInConnection{

	private int gameTick;
	private String gameId;
	private List<CarPosition> carPositions;
	
	public InsertLog(int gameTick, String gameId, List<CarPosition> carPositions){
		this.gameTick = gameTick;
		this.gameId = gameId;
		this.carPositions = carPositions;
	}
	
	public int exec(){
		return (Integer) doSql(this);
	}
	
	@Override
	public Integer action(Connection conn) throws Exception{
		String sql = "replace into log values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement stat = conn.prepareStatement(sql);
		for (int index = 0; index < carPositions.size(); index++) {
			int i = 1;
			CarPosition carPosition = carPositions.get(index);

			stat.setString(i++, gameId);
			stat.setString(i++, carPosition.id.name);
			stat.setInt(i++, gameTick);
			stat.setInt(i++, carPosition.piecePosition.lap);
			stat.setInt(i++, carPosition.piecePosition.pieceIndex);
			stat.setDouble(i++, carPosition.piecePosition.inPieceDistance);
			stat.setInt(i++, carPosition.piecePosition.lane.startLaneIndex);
			stat.setInt(i++, carPosition.piecePosition.lane.endLaneIndex);
			stat.setDouble(i++, carPosition.angle);
			
			stat.setNull(i++, Types.VARCHAR);
			stat.setNull(i++, Types.VARCHAR);
			stat.setNull(i++, Types.VARCHAR);
			stat.setInt(i++, 0);
			stat.setTimestamp(i++, new Timestamp(new Date().getTime()));
			stat.addBatch();
		}
		stat.executeBatch();
		
		return stat.getUpdateCount();
	}
	
	public static void main(String[] args){
		
		String line = "{\"msgType\":\"carPositions\",\"data\":[{\"id\":{\"name\":\"Green Coder\",\"color\":\"red\"},\"angle\":0.0,\"piecePosition\":{\"pieceIndex\":0,\"inPieceDistance\":1.9603984000000003,\"lane\":{\"startLaneIndex\":0,\"endLaneIndex\":0},\"lap\":0}}],\"gameId\":\"0bbe006b-7721-4f4e-9ccb-2d237a5e2970\",\"gameTick\":4}";
		
		ReceiveMsg<List<CarPosition>> receiveMsg = JsonParseUtil.carPosition(line);
		InsertLog replace = new InsertLog(receiveMsg.gameTick, receiveMsg.gameId, receiveMsg.data);
		int res = replace.exec();
		System.out.println(res);
	}
}
