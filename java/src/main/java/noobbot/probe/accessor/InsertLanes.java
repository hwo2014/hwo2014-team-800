package noobbot.probe.accessor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import noobbot.common.JsonParseUtil;
import noobbot.entity.msg.receive.GameInit;
import noobbot.entity.msg.receive.GameInit.Race.Track.Lane;
import noobbot.entity.msg.receive.ReceiveMsg;
import noobbot.probe.accessor.BaseAccessor.DoInConnection;

public class InsertLanes extends BaseAccessor implements DoInConnection{

	private String gameId;
	private List<Lane> lanes;
	
	public InsertLanes(String gameId, List<Lane> lanes){
		this.gameId = gameId;
		this.lanes = lanes;
	}
	
	public int exec(){
		return (Integer) doSql(this);
	}

	@Override
	public Integer action(Connection conn) throws Exception{
		String sql = "replace into lane values(?, ?, ?, ?)";
		PreparedStatement stat = conn.prepareStatement(sql);
		for (int index = 0; index < lanes.size(); index++) {
			int i = 1;
			Lane lane = lanes.get(index);
			stat.setString(i++, gameId);
			stat.setInt(i++, index);
			stat.setDouble(i++, lane.distanceFromCenter);
			stat.setTimestamp(i++, new Timestamp(new Date().getTime()));
			stat.addBatch();
		}
		stat.executeBatch();
		
		return stat.getUpdateCount();
	}
	
	public static void main(String[] args){
		
		String line = "{\"msgType\":\"gameInit\",\"data\":{\"race\":{\"track\":{\"id\":\"keimola\",\"name\":\"Keimola\",\"pieces\":[{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":200,\"angle\":22.5,\"switch\":true},{\"length\":100.0},{\"length\":100.0},{\"radius\":200,\"angle\":-22.5},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":200,\"angle\":22.5},{\"radius\":200,\"angle\":-22.5},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"length\":62.0},{\"radius\":100,\"angle\":-45.0,\"switch\":true},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"length\":100.0,\"switch\":true},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":90.0}],\"lanes\":[{\"distanceFromCenter\":-10,\"index\":0},{\"distanceFromCenter\":10,\"index\":1}],\"startingPoint\":{\"position\":{\"x\":-300.0,\"y\":-44.0},\"angle\":90.0}},\"cars\":[{\"id\":{\"name\":\"Green Coder\",\"color\":\"red\"},\"dimensions\":{\"length\":40.0,\"width\":20.0,\"guideFlagPosition\":10.0}}],\"raceSession\":{\"laps\":3,\"maxLapTimeMs\":60000,\"quickRace\":true}}},\"gameId\":\"0bbe006b-7721-4f4e-9ccb-2d237a5e2970\"}";
		
		ReceiveMsg<GameInit> receiveMsg = JsonParseUtil.gameInit(line);
		InsertLanes replace = new InsertLanes(receiveMsg.gameId, receiveMsg.data.race.track.lanes);
		int res = replace.exec();
		System.out.println(res);
	}
}
