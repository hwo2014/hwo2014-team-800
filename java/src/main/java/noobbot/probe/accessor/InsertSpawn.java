package noobbot.probe.accessor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.Date;

import noobbot.entity.msg.receive.CarId;
import noobbot.probe.accessor.BaseAccessor.DoInConnection;

public class InsertSpawn extends BaseAccessor implements DoInConnection{

	private String gameId;
	private int tick;
	private CarId carId;
	
	public InsertSpawn(String gameId, int tick, CarId carId){
		this.gameId = gameId;
		this.tick = tick;
		this.carId = carId;
	}
	
	public int exec(){
		return (Integer) doSql(this);
	}
	
	@Override
	public Integer action(Connection conn) throws Exception{
		String sql = "replace into spawn values(?, ?, ?, ?)";
		PreparedStatement stat = conn.prepareStatement(sql);
		int i = 1;
		stat.setString(i++, gameId);
		stat.setString(i++, carId.name);
		stat.setInt(i++, tick);
		stat.setTimestamp(i++, new Timestamp(new Date().getTime()));
		return stat.executeUpdate();
	}
}
