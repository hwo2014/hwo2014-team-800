package noobbot.probe.accessor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.Date;

import noobbot.entity.msg.receive.TurboAvailable;
import noobbot.probe.accessor.BaseAccessor.DoInConnection;

public class InsertTurboAvailable extends BaseAccessor implements DoInConnection{

	private String gameId;
	private int gameTick;
	private TurboAvailable turboAvailable;
	
	public InsertTurboAvailable(String gameId, int gameTick, TurboAvailable turboAvailable){
		this.gameId = gameId;
		this.gameTick = gameTick;
		this.turboAvailable = turboAvailable;
	}
	
	public int exec(){
		return (Integer) doSql(this);
	}
	
	@Override
	public Integer action(Connection conn) throws Exception{
		String sql = "replace into turboAvailable values(?, ?, ?, ?, ?, ?)";
		PreparedStatement stat = conn.prepareStatement(sql);
		int i = 1;
		stat.setString(i++, gameId);
		stat.setInt(i++, gameTick);
		stat.setDouble(i++, turboAvailable.turboDurationMilliseconds);
		stat.setInt(i++, turboAvailable.turboDurationTicks);
		stat.setDouble(i++, turboAvailable.turboFactor);
		stat.setTimestamp(i++, new Timestamp(new Date().getTime()));
		return stat.executeUpdate();
	}
}
