package noobbot.probe.accessor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import noobbot.entity.dao.DaoTurboAvailable;
import noobbot.probe.accessor.BaseAccessor.DoInConnection;

public class SelectTurboAvailableByGameId extends BaseAccessor implements DoInConnection{

	private String gameId;
	
	public SelectTurboAvailableByGameId(String gameId){
		this.gameId = gameId;
	}
	
	@SuppressWarnings("unchecked")
	public List<DaoTurboAvailable> exec(){
		return (List<DaoTurboAvailable>) doSql(this);
	}
	
	@Override
	public List<DaoTurboAvailable> action(Connection conn) throws Exception{
		String sql = "select * from turboAvailable where gameId = ?";
		PreparedStatement stat = conn.prepareStatement(sql);
		int i = 1;
		stat.setString(i++, gameId);
		
		ResultSet rs = stat.executeQuery();
		List<DaoTurboAvailable> result = new ArrayList<DaoTurboAvailable>();
		while(rs.next()){
			DaoTurboAvailable log = toTurboAvailable(rs);
			result.add(log);
		}
		return result;
	}
	
	public static DaoTurboAvailable toTurboAvailable(ResultSet rs) throws Exception{
		DaoTurboAvailable entity = new DaoTurboAvailable();
		entity.gameId = rs.getString("gameId");
		entity.gameTick   = rs.getInt("gameTick");
		entity.turboDurationMilliseconds   = rs.getDouble("turboDurationMilliseconds");
		entity.turboDurationTicks   = rs.getInt("turboDurationTicks");
		entity.turboFactor   = rs.getDouble("turboFactor");
		entity.updateTime = rs.getDate("update_time");
		return entity;
	}
}
