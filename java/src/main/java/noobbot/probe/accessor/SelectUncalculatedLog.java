package noobbot.probe.accessor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import noobbot.entity.dao.DaoLog;
import noobbot.probe.accessor.BaseAccessor.DoInConnection;

public class SelectUncalculatedLog extends BaseAccessor implements DoInConnection{

	private String gameId;
	private String carName;
	private int offset;
	private int limit;
	
	public SelectUncalculatedLog(String gameId, String carName){
		this(gameId, carName, 0, Integer.MAX_VALUE);
	}
	
	public SelectUncalculatedLog(String gameId, String carName, int offset, int limit){
		this.gameId = gameId;
		this.carName = carName;
		this.offset = offset;
		this.limit = limit;
	}
	
	@SuppressWarnings("unchecked")
	public List<DaoLog> exec(){
		return (List<DaoLog>) doSql(this);
	}
	
	@Override
	public List<DaoLog> action(Connection conn) throws Exception{
		String sql = "select * from log where (velocity is null or acceleration is null " +
						"or isCrash is null) and gameId = ? and carName = ? " + 
						"order by gameId, carName, gameTick limit ?, ?";
		PreparedStatement stat = conn.prepareStatement(sql);
		int i = 1;
		stat.setString(i++, gameId);
		stat.setString(i++, carName);
		stat.setInt(i++, offset);
		stat.setInt(i++, limit);
		
		ResultSet rs = stat.executeQuery();
		List<DaoLog> result = new ArrayList<DaoLog>();
		while(rs.next()){
			DaoLog log = toLog(rs);
			result.add(log);
		}
		return result;
	}
	
	public static DaoLog toLog(ResultSet rs) throws Exception{
		DaoLog log = new DaoLog();
		log.gameId = rs.getString("gameId");
		log.carName = rs.getString("carName");
		log.gameTick = rs.getInt("gameTick");
		log.lap = rs.getInt("lap");
		log.pieceIndex = rs.getInt("pieceIndex");
		log.inPieceDistance = rs.getDouble("inPieceDistance");
		log.startLaneIndex = rs.getInt("startLaneIndex");
		log.endLaneIndex = rs.getInt("endLaneIndex");
		log.throttle = rs.getDouble("throttle");
		log.velocity = rs.getDouble("velocity");
		if (rs.wasNull()) {
			log.velocity = null;
		}
		log.acceleration = rs.getDouble("acceleration");
		if (rs.wasNull()) {
			log.acceleration = null;
		}
		log.isCrash = rs.getBoolean("isCrash");
		if (rs.wasNull()) {
			log.isCrash = null;
		}
		log.update_time = rs.getDate("update_time");
		return log;
	}
	
	public static void main(String[] args){
		String gameId = "6714ea0d-0dc5-434a-adea-67cfe5ae190c";
		String carName = "Green Coder";

		SelectUncalculatedLog selectUncalculatedLog = new SelectUncalculatedLog(gameId, carName, 0, 99999999);
		List<DaoLog> res = selectUncalculatedLog.exec();
		System.out.println(res.size());
	}
}
