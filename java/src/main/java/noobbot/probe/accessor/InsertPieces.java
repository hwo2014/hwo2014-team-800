package noobbot.probe.accessor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Date;
import java.util.List;

import noobbot.common.JsonParseUtil;
import noobbot.entity.msg.receive.GameInit;
import noobbot.entity.msg.receive.GameInit.Race.Track.Piece;
import noobbot.entity.msg.receive.ReceiveMsg;
import noobbot.probe.accessor.BaseAccessor.DoInConnection;

public class InsertPieces extends BaseAccessor implements DoInConnection{

	private String trackId;
	private List<Piece> pieces;
	
	public InsertPieces(String trackId, List<Piece> pieces){
		this.trackId = trackId;
		this.pieces = pieces;
	}
	
	public int exec(){
		return (Integer) doSql(this);
	}
	
	@Override
	public Integer action(Connection conn) throws Exception{
		String sql = "replace into piece values(?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement stat = conn.prepareStatement(sql);
		for (int index = 0; index < pieces.size(); index++) {
			int i = 1;
			Piece piece = pieces.get(index);
			stat.setString(i++, trackId);
			stat.setInt(i++, index);
			if (piece.length == null){
				stat.setNull(i++, Types.VARCHAR);
			}else{
				stat.setDouble(i++, piece.length);
			}
			if (piece.angle == null){
				stat.setNull(i++, Types.VARCHAR);
			}else{
				stat.setDouble(i++, piece.angle);
			}
			if (piece.radius == null){
				stat.setNull(i++, Types.VARCHAR);
			}else{
				stat.setDouble(i++, piece.radius);
			}
			if (piece.hasSwitch == null){
				stat.setBoolean(i++, false);
			}else{
				stat.setBoolean(i++, piece.hasSwitch);
			}
			stat.setTimestamp(i++, new Timestamp(new Date().getTime()));
			stat.addBatch();
		}
		stat.executeBatch();
		
		return stat.getUpdateCount();
	}
	
	public static void main(String[] args){
		
		String line = "{\"msgType\":\"gameInit\",\"data\":{\"race\":{\"track\":{\"id\":\"keimola\",\"name\":\"Keimola\",\"pieces\":[{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":200,\"angle\":22.5,\"switch\":true},{\"length\":100.0},{\"length\":100.0},{\"radius\":200,\"angle\":-22.5},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":200,\"angle\":22.5},{\"radius\":200,\"angle\":-22.5},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"length\":62.0},{\"radius\":100,\"angle\":-45.0,\"switch\":true},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"length\":100.0,\"switch\":true},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":90.0}],\"lanes\":[{\"distanceFromCenter\":-10,\"index\":0},{\"distanceFromCenter\":10,\"index\":1}],\"startingPoint\":{\"position\":{\"x\":-300.0,\"y\":-44.0},\"angle\":90.0}},\"cars\":[{\"id\":{\"name\":\"Green Coder\",\"color\":\"red\"},\"dimensions\":{\"length\":40.0,\"width\":20.0,\"guideFlagPosition\":10.0}}],\"raceSession\":{\"laps\":3,\"maxLapTimeMs\":60000,\"quickRace\":true}}},\"gameId\":\"0bbe006b-7721-4f4e-9ccb-2d237a5e2970\"}";
		
		ReceiveMsg<GameInit> receiveMsg = JsonParseUtil.gameInit(line);
		InsertPieces replace = new InsertPieces(receiveMsg.gameId, receiveMsg.data.race.track.pieces);
		int res = replace.exec();
		System.out.println(res);
	}
}
