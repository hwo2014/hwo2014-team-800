package noobbot.probe.accessor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import noobbot.entity.dao.DaoCrash;
import noobbot.probe.accessor.BaseAccessor.DoInConnection;

public class SelectCrashByCar extends BaseAccessor implements DoInConnection{

	private String gameId;
	private String carName;
	
	public SelectCrashByCar(String gameId, String carName){
		this.gameId = gameId;
		this.carName = carName;
	}
	
	@SuppressWarnings("unchecked")
	public List<DaoCrash> exec(){
		return (List<DaoCrash>) doSql(this);
	}
	
	@Override
	public List<DaoCrash> action(Connection conn) throws Exception{
		String sql = "select * from crash " + 
						"where gameId = ? and carName = ? ";
		PreparedStatement stat = conn.prepareStatement(sql);
		int i = 1;
		stat.setString(i++, gameId);
		stat.setString(i++, carName);
		
		ResultSet rs = stat.executeQuery();
		List<DaoCrash> result = new ArrayList<DaoCrash>();
		while(rs.next()){
			DaoCrash log = toCrash(rs);
			result.add(log);
		}
		return result;
	}
	
	public static DaoCrash toCrash(ResultSet rs) throws Exception{
		DaoCrash log = new DaoCrash();
		log.gameId = rs.getString("gameId");
		log.carName = rs.getString("carName");
		log.gameTick   = rs.getInt("gameTick");
		log.updateTime = rs.getDate("update_time");
		return log;
	}
}
