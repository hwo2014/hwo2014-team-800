package noobbot.probe.accessor;

import java.sql.Connection;
import java.sql.DriverManager;

public class BaseAccessor {
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost/hwo2014_acc";
	static final String USER = "root";
	static final String PASS = "";
	static {
		try {
			Class.forName(JDBC_DRIVER);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public interface DoInConnection{
		Object action(Connection conn) throws Exception;
	}

	public Object doSql(DoInConnection doin) {
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			return doin.action(conn);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			if (conn != null)
				try {
					conn.close();
				} catch (Exception e) {
			}
		}
	}
}
