create table piece (
  gameId varchar(100),
  pieceIndex int,
  length double,
  angle double,
  radius double,
  hasSwitch tinyint,
  update_time datetime,
  primary key (gameId, pieceIndex)
);

create table lane (
  gameId varchar(100),
  laneIndex int,
  distanceFromCenter double,
  update_time datetime,
  primary key (gameId, laneIndex)
);

create table log (
  gameId varchar(100),
  carName varchar(100),
  gameTick int,
  lap int,
  pieceIndex int,
  inPieceDistance double,
  startLaneIndex int,
  endLaneIndex int,
  angle double,
  throttle double,
  velocity double,
  acceleration double,
  isCrash tinyint,
  update_time datetime,
  primary key (gameId, carName, gameTick)
);

create table crash (
  gameId varchar(100),
  carName varchar(100),
  gameTick int,
  update_time datetime,
  primary key (gameId, carName, gameTick)
);

create table spawn (
  gameId varchar(100),
  carName varchar(100),
  gameTick int,
  update_time datetime,
  primary key (gameId, carName, gameTick)
);

create table turboAvailable (
  gameId varchar(100),
  gameTick int,
  turboDurationMilliseconds double,
  turboDurationTicks int,
  turboFactor double,
  update_time datetime,
  primary key (gameId, gameTick)
);

/*
drop table crash;
drop table lane;
drop table log;
drop table piece;
drop table spawn;
drop table turboAvailable;

truncate table crash;
truncate table lane;
truncate table log;
truncate table piece;
truncate table spawn;
truncate table turboAvailable;

select count(*) from crash;
select count(*) from lane;
select count(*) from log;
select count(*) from piece;
select count(*) from spawn;
select count(*) from turboAvailable;

select count(*) from log where velocity is null;

update log set velocity = null, acceleration = null, isCrash = null;

select * from log where throttle != 1 
  into outfile '/tmp/log.csv' fields terminated by ',' enclosed by '"' lines terminated by '\n';

gameId	carName	gameTick	lap	pieceIndex	inPieceDistance	startLaneIndex	endLaneIndex	angle	throttle	velocity	acceleration	isCrash	update_time



 */