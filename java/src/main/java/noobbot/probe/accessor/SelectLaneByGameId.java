package noobbot.probe.accessor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import noobbot.entity.dao.DaoLane;
import noobbot.probe.accessor.BaseAccessor.DoInConnection;

public class SelectLaneByGameId extends BaseAccessor implements DoInConnection{

	private String gameId;
	
	public SelectLaneByGameId(String gameId){
		this.gameId = gameId;
	}
	
	@SuppressWarnings("unchecked")
	public List<DaoLane> exec(){
		return (List<DaoLane>) doSql(this);
	}
	
	@Override
	public List<DaoLane> action(Connection conn) throws Exception{
		String sql = "select * from lane " + 
						"where gameId = ? order by laneIndex";
		PreparedStatement stat = conn.prepareStatement(sql);
		int i = 1;
		stat.setString(i++, gameId);
		
		ResultSet rs = stat.executeQuery();
		List<DaoLane> result = new ArrayList<DaoLane>();
		while(rs.next()){
			DaoLane log = toLane(rs);
			result.add(log);
		}
		return result;
	}
	
	public static DaoLane toLane(ResultSet rs) throws Exception{
		DaoLane log = new DaoLane();
		log.gameId = rs.getString("gameId");
		log.laneIndex = rs.getInt("laneIndex");
		log.distanceFromCenter = rs.getDouble("distanceFromCenter");
		log.updateTime = rs.getDate("update_time");
		return log;
	}
}
