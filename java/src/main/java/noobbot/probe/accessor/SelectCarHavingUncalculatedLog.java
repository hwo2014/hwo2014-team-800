package noobbot.probe.accessor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import noobbot.probe.accessor.BaseAccessor.DoInConnection;

public class SelectCarHavingUncalculatedLog extends BaseAccessor implements DoInConnection{

	private int offset;
	private int limit;
	
	public SelectCarHavingUncalculatedLog(){
		this(0, 99999999);
	}
	
	public SelectCarHavingUncalculatedLog(int offset, int limit){
		this.offset = offset;
		this.limit = limit;
	}
	
	@SuppressWarnings("unchecked")
	public List<CarInGame> exec(){
		return (List<CarInGame>) doSql(this);
	}
	
	@Override
	public List<CarInGame> action(Connection conn) throws Exception{
		String sql = "select distinct gameId, carName from log " + 
						"where velocity is null or acceleration is null " +
						"or isCrash is null limit ?, ?";
		PreparedStatement stat = conn.prepareStatement(sql);
		int i = 1;
		stat.setInt(i++, offset);
		stat.setInt(i++, limit);
		
		ResultSet rs = stat.executeQuery();
		List<CarInGame> result = new ArrayList<CarInGame>();
		while(rs.next()){
			CarInGame log = toCarOfTrack(rs);
			result.add(log);
		}
		return result;
	}
	
	public static CarInGame toCarOfTrack(ResultSet rs) throws Exception{
		CarInGame log = new CarInGame();
		log.gameId = rs.getString("gameId");
		log.carName = rs.getString("carName");
		return log;
	}
	
	public static class CarInGame{
		public String gameId;
		public String carName;
	}
	
	public static void main(String[] args){
		SelectCarHavingUncalculatedLog selectUncalculatedLog = new SelectCarHavingUncalculatedLog(0, 9999999);
		List<CarInGame> res = selectUncalculatedLog.exec();
		System.out.println(res.size());
	}
}
