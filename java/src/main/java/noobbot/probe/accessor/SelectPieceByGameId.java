package noobbot.probe.accessor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import noobbot.entity.dao.DaoPiece;
import noobbot.probe.accessor.BaseAccessor.DoInConnection;

public class SelectPieceByGameId extends BaseAccessor implements DoInConnection{

	private String gameId;
	
	public SelectPieceByGameId(String gameId){
		this.gameId = gameId;
	}
	
	@SuppressWarnings("unchecked")
	public List<DaoPiece> exec(){
		return (List<DaoPiece>) doSql(this);
	}
	
	@Override
	public List<DaoPiece> action(Connection conn) throws Exception{
		String sql = "select * from piece " + 
						"where gameId = ? order by pieceIndex";
		PreparedStatement stat = conn.prepareStatement(sql);
		int i = 1;
		stat.setString(i++, gameId);
		
		ResultSet rs = stat.executeQuery();
		List<DaoPiece> result = new ArrayList<DaoPiece>();
		while(rs.next()){
			DaoPiece log = toPiece(rs);
			result.add(log);
		}
		return result;
	}
	
	public static DaoPiece toPiece(ResultSet rs) throws Exception{
		DaoPiece piece = new DaoPiece();
		piece.gameId = rs.getString("gameId");
		piece.pieceIndex = rs.getInt("pieceIndex");
		piece.length = rs.getDouble("length");
		if (rs.wasNull()) {
			piece.length = null;
		}
		piece.angle = rs.getDouble("angle");
		if (rs.wasNull()) {
			piece.angle = null;
		}
		piece.radius = rs.getDouble("radius");
		if (rs.wasNull()) {
			piece.radius = null;
		}
		piece.hasSwitch = rs.getBoolean("hasSwitch");
		if (rs.wasNull()) {
			piece.hasSwitch = null;
		}
		piece.updateTime = rs.getDate("update_time");
		return piece;
	}
}
