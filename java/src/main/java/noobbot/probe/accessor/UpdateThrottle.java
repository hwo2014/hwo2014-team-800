package noobbot.probe.accessor;

import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;

import noobbot.common.Constant;
import noobbot.entity.msg.receive.CarId;
import noobbot.entity.msg.receive.CarPosition;
import noobbot.entity.msg.receive.ReceiveMsg;
import noobbot.probe.accessor.BaseAccessor.DoInConnection;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

public class UpdateThrottle extends BaseAccessor implements DoInConnection{

	private int gameTick;
	private String gameId;
	private CarId carId;
	private double throttle;
	
	
	public UpdateThrottle(int gameTick, String gameId, double throttle){
		this.gameTick = gameTick;
		this.gameId = gameId;
		this.carId = new CarId();
		this.carId.name = Constant.botName;
		this.throttle = throttle;
	}
	
	public UpdateThrottle(int gameTick, String gameId, CarId carId, double throttle){
		this.gameTick = gameTick;
		this.gameId = gameId;
		this.carId = carId;
		this.throttle = throttle;
	}
	
	public int exec(){
		return (Integer) doSql(this);
	}
	
	@Override
	public Integer action(Connection conn) throws Exception{
		String sql = "update log set throttle = ? where gameTick = ? and gameId = ? and carName = ?";
		PreparedStatement stat = conn.prepareStatement(sql);
		int i = 1;
		
		stat.setDouble(i++, throttle);
		stat.setInt(i++, gameTick);
		stat.setString(i++, gameId);
		stat.setString(i++, carId.name);
		
		return stat.executeUpdate();
	}
	
	public static void main(String[] args){
		
		String line = "{\"msgType\":\"carPositions\",\"data\":[{\"id\":{\"name\":\"Green Coder\",\"color\":\"red\"},\"angle\":0.0,\"piecePosition\":{\"pieceIndex\":0,\"inPieceDistance\":1.9603984000000003,\"lane\":{\"startLaneIndex\":0,\"endLaneIndex\":0},\"lap\":0}}],\"gameId\":\"0bbe006b-7721-4f4e-9ccb-2d237a5e2970\",\"gameTick\":4}";
		
		@SuppressWarnings("serial")
		Type type = new TypeToken<ReceiveMsg<List<CarPosition>>>(){}.getType();
		Gson gson = new Gson();
		ReceiveMsg<List<CarPosition>> receiveMsg = gson.fromJson(line, type);
		UpdateThrottle replace = new UpdateThrottle(receiveMsg.gameTick, receiveMsg.gameId, receiveMsg.data.get(0).id, 0.6d);
		int res = replace.exec();
		System.out.println(res);
	}
}
