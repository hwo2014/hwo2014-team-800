package noobbot.probe.parse;

import java.io.File;
import java.io.IOException;
import java.util.List;

import noobbot.common.JsonParseUtil;
import noobbot.entity.msg.receive.CarId;
import noobbot.entity.msg.receive.CarPosition;
import noobbot.entity.msg.receive.GameInit;
import noobbot.entity.msg.receive.ReceiveMsg;
import noobbot.entity.msg.receive.TurboAvailable;
import noobbot.probe.accessor.InsertCrash;
import noobbot.probe.accessor.InsertLanes;
import noobbot.probe.accessor.InsertLog;
import noobbot.probe.accessor.InsertPieces;
import noobbot.probe.accessor.InsertSpawn;
import noobbot.probe.accessor.InsertTurboAvailable;
import noobbot.probe.accessor.UpdateThrottle;

import org.apache.commons.io.FileUtils;

public class LogPaser {
	
	private ReceiveMsg<GameInit> gameInit;
	
	public LogPaser(String filePath) throws IOException{
		File file = new File(filePath);
		@SuppressWarnings("unchecked")
		List<String> lines = FileUtils.readLines(file, "UTF-8");
		ReceiveMsg<List<CarPosition>> prevPos = null;
		for (int i = 0; i < lines.size(); i++){
			String line = lines.get(i);
			if (isGameInit(line)){
				saveGameInit(line);
			}else if (isCrash(line)){
				saveCrash(line);
			}else if (isSpawn(line)){
				saveSpawn(line);
			}else if (isCarPosition(line)){
				prevPos = saveCarPosition(line);
			}else if (isTurboAvailable(line)){
				saveTurboAvailable(line);
			}else if (isThrottle(line)){
				saveThrottle(prevPos, line);
			}
		}
	}
	
	private boolean isGameInit(String line){
		return line.startsWith("R {\"msgType\":\"gameInit");
	}
	
	private void saveGameInit(String line){
		gameInit = JsonParseUtil.gameInit(line.substring(2));
		InsertPieces replacePieces = new InsertPieces(gameInit.gameId, gameInit.data.race.track.pieces);
		replacePieces.exec();
		InsertLanes replaceLanes = new InsertLanes(gameInit.gameId, gameInit.data.race.track.lanes);
		replaceLanes.exec();
	}
	
	private boolean isCrash(String line){
		return line.startsWith("R {\"msgType\":\"crash\"");
	}
	
	private void saveCrash(String line){
		ReceiveMsg<CarId> msg = JsonParseUtil.crash(line.substring(2));
		InsertCrash insertCrash = new InsertCrash(msg.gameId, msg.gameTick, msg.data);
		insertCrash.exec();
	}
	
	private boolean isSpawn(String line){
		return line.startsWith("R {\"msgType\":\"spawn\"");
	}
	
	private void saveSpawn(String line){
		ReceiveMsg<CarId> msg = JsonParseUtil.spawn(line.substring(2));
		InsertSpawn insertSpawn = new InsertSpawn(msg.gameId, msg.gameTick, msg.data);
		insertSpawn.exec();
	}
	
	private boolean isTurboAvailable(String line){
		return line.startsWith("R {\"msgType\":\"turboAvailable\"");
	}
	
	private void saveTurboAvailable(String line){
		ReceiveMsg<TurboAvailable> msg = JsonParseUtil.turboAvailable(line.substring(2));
		InsertTurboAvailable insert = new InsertTurboAvailable(msg.gameId, msg.gameTick, msg.data);
		insert.exec();
	}
	
	private boolean isCarPosition(String line){
		return line.startsWith("R {\"msgType\":\"carPositions");
	}
	
	private boolean isThrottle(String line){
		return line.startsWith("S {\"msgType\":\"throttle");
	}
	
	private ReceiveMsg<List<CarPosition>> saveCarPosition(String line){
		ReceiveMsg<List<CarPosition>> pos = JsonParseUtil.carPosition(line.substring(2));
		InsertLog replaceLog = new InsertLog(pos.gameTick, pos.gameId, pos.data);
		replaceLog.exec();
		return pos;
	}
	
	private void saveThrottle(ReceiveMsg<List<CarPosition>> pos, String line){
		double throttle = parseThrottle(line);
		UpdateThrottle updateThrottle = new UpdateThrottle(pos.gameTick, pos.gameId, throttle);
		updateThrottle.exec();
	}
	
	private double parseThrottle(String line){
		String value = line.replace("S {\"msgType\":\"throttle\",\"data\":", "").replace("}", "");
		return Double.valueOf(value);
	}
	
	public static void main(String[] args) throws IOException{
		new LogPaser(args[0]);
	}
}
