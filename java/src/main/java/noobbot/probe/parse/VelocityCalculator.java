package noobbot.probe.parse;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import noobbot.entity.dao.DaoCrash;
import noobbot.entity.dao.DaoLane;
import noobbot.entity.dao.DaoLog;
import noobbot.entity.dao.DaoPiece;
import noobbot.entity.dao.DaoTurboAvailable;
import noobbot.probe.accessor.SelectCarHavingUncalculatedLog;
import noobbot.probe.accessor.SelectCarHavingUncalculatedLog.CarInGame;
import noobbot.probe.accessor.SelectCrashByCar;
import noobbot.probe.accessor.SelectLaneByGameId;
import noobbot.probe.accessor.SelectPieceByGameId;
import noobbot.probe.accessor.SelectTurboAvailableByGameId;
import noobbot.probe.accessor.SelectUncalculatedLog;
import noobbot.probe.accessor.UpdateVelocity;

public class VelocityCalculator {
	
	private List<DaoLog> logs;
	private Map<Integer, DaoPiece> pieceMap;
	private Map<Integer, DaoLane> laneMap;
	private Map<Integer, DaoCrash> crashMap;
	@SuppressWarnings("unused")
	private List<DaoTurboAvailable> turbos;
	
	public VelocityCalculator() throws IOException{
		SelectCarHavingUncalculatedLog selectCarInGame = new SelectCarHavingUncalculatedLog();
		List<CarInGame> cars = selectCarInGame.exec();
		for (CarInGame car : cars){
			cashGameInfo(car);
			parseACar(car);
		}
	}
	
	private void cashGameInfo(CarInGame car){
		SelectUncalculatedLog selectUncalc = new SelectUncalculatedLog(car.gameId, car.carName);
		logs = selectUncalc.exec();
		SelectLaneByGameId selectLane = new SelectLaneByGameId(car.gameId);
		laneMap = new HashMap<Integer, DaoLane>();
		for (DaoLane lane : selectLane.exec()){
			laneMap.put(lane.laneIndex, lane);
		}
		SelectPieceByGameId selectPiece = new SelectPieceByGameId(car.gameId);
		pieceMap = new HashMap<Integer, DaoPiece>();
		for (DaoPiece piece : selectPiece.exec()){
			pieceMap.put(piece.pieceIndex, piece);
		}
		SelectCrashByCar selectCrash = new SelectCrashByCar(car.gameId, car.carName);
		crashMap = new HashMap<Integer, DaoCrash>();
		for (DaoCrash crash : selectCrash.exec()){
			crashMap.put(crash.gameTick, crash);
		}
		SelectTurboAvailableByGameId selectTurbo = new SelectTurboAvailableByGameId(car.gameId);
		turbos = selectTurbo.exec();
	}
	
	private void parseACar(CarInGame car){
		for (DaoLog log : logs){
			if (log.velocity == null){
				log.velocity = calcVelocity(log);
				log.isCrash = crashMap.containsKey(log.gameTick + 1) || crashMap.containsKey(log.gameTick + 2);
			}
		}
		for (DaoLog log : logs){
			if (log.acceleration == null){
				log.acceleration = calcAcceleration(log);
			}
		}
		for (DaoLog log : logs){
			UpdateVelocity updater = new UpdateVelocity(log);
			updater.exec();
		}
	}
	
	private double calcVelocity(DaoLog log){
		if (log == logs.get(logs.size() - 1)){
			return 0;
		}
		DaoLog next = logs.get(logs.indexOf(log) + 1);

		if (log.pieceIndex == next.pieceIndex){
        	return next.inPieceDistance - log.inPieceDistance;
		}else{
			double pieceLength = getPieceLength(log);
			return pieceLength - log.inPieceDistance + next.inPieceDistance;
		}
	}
	
	private double getPieceLength(DaoLog log){
		DaoPiece piece = pieceMap.get(log.pieceIndex);
		if (piece.length != null){
			return piece.length;
		}
		double distanceFromCenter = laneMap.get(log.startLaneIndex).distanceFromCenter;
		double angle = piece.angle;
		if (0 < piece.angle){
			distanceFromCenter = -1 * distanceFromCenter;
		}else{
			angle = -1 * angle;
		}
		double radius = piece.radius + distanceFromCenter;
		return radius * 2 * Math.PI * angle / 360;
	}
	
	private double calcAcceleration(DaoLog log){
		double nextVelocity = 0;
		if (log != logs.get(logs.size() - 1)){
			DaoLog next = logs.get(logs.indexOf(log) + 1);
			if (! next.isCrash){
				nextVelocity = next.velocity;
			}
		}
		return nextVelocity - log.velocity;
	}
	
	public static void main(String[] args) throws Exception{
		new VelocityCalculator();
	}
}
