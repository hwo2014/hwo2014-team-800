package noobbot.probe.accumulate;

import noobbot.common.Constant;
import noobbot.connector.Connector;
import noobbot.connector.RealConnector;
import noobbot.controller.Controller;
import noobbot.strategy.DecelerationTestStrategy;

public class DeceleratingDataAccumulater {
	
	public static void main(String[] args){
        System.out.println("DeceleratingDataAccumulater start");
        
        Connector connector = new RealConnector(Constant.Servers.testserver, Constant.port, Constant.botName, Constant.botKey);
        for (int i = 0; i < 100; i++){
        	try{
        		double throttle = (double) i / 100;
                new Controller(connector, new DecelerationTestStrategy(throttle));
        	}catch(Exception e){
        		e.printStackTrace();
        	}
        }
	}
}
