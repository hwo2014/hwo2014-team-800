package noobbot.probe.accumulate;

import noobbot.common.Constant;
import noobbot.connector.Connector;
import noobbot.connector.RealConnector;
import noobbot.controller.Controller;
import noobbot.strategy.ConstantThrottleStrategy;

public class StraightAcceleratingDataAccumulater {
	
	public static void main(String[] args){
        System.out.println("StraightDataAccumulate start");
        
        Connector connector = new RealConnector(Constant.Servers.testserver, Constant.port, Constant.botName, Constant.botKey);
        for (int i = 0; i < 38; i++){
        	try{
        		double throttle = (double) (100 - i) / 100;
                new Controller(connector, new ConstantThrottleStrategy(throttle));
        	}catch(Exception e){
        		e.printStackTrace();
        	}
        }
	}
}
