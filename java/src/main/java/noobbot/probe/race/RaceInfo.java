package noobbot.probe.race;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import noobbot.entity.msg.receive.GameInit;
import noobbot.entity.msg.receive.ReceiveMsg;
import noobbot.entity.msg.receive.GameInit.Race;
import noobbot.probe.race.RaceInfo;

@Deprecated
public class RaceInfo {
	
	private static RaceInfo instance = new RaceInfo();
	public Map<String, Race> data = new HashMap<String, Race>();
	
	public static RaceInfo getInstance(){
		return instance;
	}
	
	private RaceInfo(){
		String finlandLine = "{\"msgType\":\"gameInit\",\"data\":{\"race\":{\"track\":{\"id\":\"keimola\",\"name\":\"Keimola\",\"pieces\":[{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":200,\"angle\":22.5,\"switch\":true},{\"length\":100.0},{\"length\":100.0},{\"radius\":200,\"angle\":-22.5},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":200,\"angle\":22.5},{\"radius\":200,\"angle\":-22.5},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"length\":62.0},{\"radius\":100,\"angle\":-45.0,\"switch\":true},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"length\":100.0,\"switch\":true},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":90.0}],\"lanes\":[{\"distanceFromCenter\":-10,\"index\":0},{\"distanceFromCenter\":10,\"index\":1}],\"startingPoint\":{\"position\":{\"x\":-300.0,\"y\":-44.0},\"angle\":90.0}},\"cars\":[{\"id\":{\"name\":\"Green Coder\",\"color\":\"red\"},\"dimensions\":{\"length\":40.0,\"width\":20.0,\"guideFlagPosition\":10.0}}],\"raceSession\":{\"laps\":3,\"maxLapTimeMs\":60000,\"quickRace\":true}}},\"gameId\":\"a3fe1cde-8d18-4793-a96d-c240e394c2d5\"}";
		data.put("finland", parseJson(finlandLine));
	}
	
	private Race parseJson(String line){
		Gson gson = new Gson();
		@SuppressWarnings("serial")
		Type type = new TypeToken<ReceiveMsg<GameInit>>(){}.getType();
		ReceiveMsg<GameInit> receiveMsg = gson.fromJson(line, type);
		return receiveMsg.data.race;
	}
	
	public static void main(String[] args){
		Race race = getInstance().data.get("finland");
		System.out.println(race.toString());
	}
}
