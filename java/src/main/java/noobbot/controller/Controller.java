package noobbot.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import noobbot.common.JsonParseUtil;
import noobbot.connector.Connector;
import noobbot.connector.ReceiveEvent;
import noobbot.entity.Operation;
import noobbot.entity.msg.receive.CarId;
import noobbot.entity.msg.receive.CarPosition;
import noobbot.entity.msg.receive.GameInit;
import noobbot.entity.msg.receive.ReceiveMsg;
import noobbot.entity.msg.send.Ping;
import noobbot.entity.msg.send.SendMsg;
import noobbot.entity.msg.send.Throttle;
import noobbot.strategy.Strategy;

public class Controller{

	private Connector connector;
	private Strategy strategy;
    
    public CarId myCar;
    public GameInit gameInit;
	
	public Controller(Connector connector, Strategy strategy) throws IOException{
		this.connector = connector;
		this.strategy = strategy;
		
		ReceiveEvent receiveEvent = new ReceiveEvent(){
			@Override
			public void receive(String line) {
				gameControll(line);
			}
		};
		connector.connect(receiveEvent);
	}
	
	private void send(SendMsg msg){
		connector.send(msg);
	}
	
	private void gameControll(String line){
		String msgTypeHead = line.substring(12, 26);
		
		// ----- on racing ---- //
		if (msgTypeHead.startsWith("carPositions")) {
			ReceiveMsg<List<CarPosition>> receiveMsg = JsonParseUtil.carPosition(line);
			Map<String, CarPosition> carPositionMap = new HashMap<String, CarPosition>();
			for (CarPosition car : receiveMsg.data){
				carPositionMap.put(car.id.name, car);
			}
			Operation operation = strategy.calcThrottle(carPositionMap, receiveMsg.gameTick);
			action(operation);
			return;
		} else if (msgTypeHead.startsWith("crash")) {
			ReceiveMsg<CarId> receiveMsg = JsonParseUtil.crash(line);
			CarId crashedCar = receiveMsg.data;
			strategy.crash(crashedCar, receiveMsg.gameTick);
			System.out.println("crash");
		} else if (msgTypeHead.startsWith("spawn")) {
			ReceiveMsg<CarId> receiveMsg = JsonParseUtil.spawn(line);
			CarId crashedCar = receiveMsg.data;
			strategy.spawn(crashedCar, receiveMsg.gameTick);
			System.out.println("spawn");
		} else if (msgTypeHead.startsWith("onLapFinished")) {
			System.out.println("onLapFinished");
		} else if (msgTypeHead.startsWith("turboAvailable")) {
			System.out.println("turboAvailable");

		// ----- game start ---- //
		} else if (msgTypeHead.startsWith("join")) {
			System.out.println("Joined");
		} else if (msgTypeHead.startsWith("gameInit")) {
			ReceiveMsg<GameInit> receiveMsg = JsonParseUtil.gameInit(line);
			gameInit = receiveMsg.data;
			System.out.println("Race init");
		} else if (msgTypeHead.startsWith("gameStart")) {
			System.out.println("Race start");
		} else if (msgTypeHead.startsWith("yourCar")) {
			ReceiveMsg<CarId> receiveMsg = JsonParseUtil.yourCar(line);
			myCar = receiveMsg.data;
			System.out.println("color of my car is " + myCar.color);

		// ----- game end ---- //
		} else if (msgTypeHead.startsWith("gameEnd")) {
			System.out.println("Race end");
		} else if (msgTypeHead.startsWith("finish")) {
			System.out.println("finish");
		} else if (msgTypeHead.startsWith("tournamentEnd")) {
			System.out.println("tournamentEnd");
		} else if (msgTypeHead.startsWith("dnf")) {
			System.out.println("dnf");
		}
		send(new Ping());
	}
	
	private void action(Operation operation){
		if (operation.throttle != null){
			send(new Throttle(operation.throttle));
		}else if (operation.isGiveUp){
			throw new RuntimeException("give up!");
		}
		if (operation.switchTo == Operation.Switch.LEFT){
			//TODO
		}else if (operation.switchTo == Operation.Switch.RIGHT){
			//TODO
		}
	}
}
