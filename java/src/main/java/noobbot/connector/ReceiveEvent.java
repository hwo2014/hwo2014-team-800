package noobbot.connector;

public interface ReceiveEvent {
	void receive(String line);
}
