package noobbot.connector;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import noobbot.entity.msg.send.Join;
import noobbot.entity.msg.send.SendMsg;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RealConnector implements Connector{

	static final Logger raceLogger = LoggerFactory.getLogger("racelog");
	
	private String host;
	private int port;
	private Join join;
	private PrintWriter writer;
	public boolean isTest = false;
	
	public RealConnector(String host, int port, String botName, String botKey){
		this.host = host;
		this.port = port;
		this.join = new Join(botName, botKey);
	}
	
	@Override
	public void connect(ReceiveEvent receiveEvent) throws IOException{
        @SuppressWarnings("resource")
		final Socket socket = new Socket(host, port);
        writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
        
        String line = null;
        send(join);

        while((line = reader.readLine()) != null) {
        	if (isTest) raceLogger.info("R " + line);
        	receiveEvent.receive(line);
        }
	}
	
	@Override
	public void send(SendMsg msg) {
    	String jsonString = msg.toJson();
    	if (isTest) raceLogger.info("S " + jsonString);
        writer.println(jsonString);
        writer.flush();
	}
}
