package noobbot.connector;

import java.io.IOException;

import noobbot.entity.msg.send.SendMsg;

public interface Connector {
	void send(SendMsg msg);
	void connect(ReceiveEvent receiveEvent) throws IOException;
}
