package noobbot.common;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Util {

	public static String dateFormatYMDHMSms(Date date){
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		return format.format(date);
	}
}
