package noobbot.common;

import java.lang.reflect.Type;
import java.util.List;

import noobbot.entity.msg.receive.CarId;
import noobbot.entity.msg.receive.CarPosition;
import noobbot.entity.msg.receive.GameInit;
import noobbot.entity.msg.receive.ReceiveMsg;
import noobbot.entity.msg.receive.TurboAvailable;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

public class JsonParseUtil {

	private static Gson gson = new Gson();
	
	public static ReceiveMsg<List<CarPosition>> carPosition(String line){
		@SuppressWarnings("serial")
		Type type = new TypeToken<ReceiveMsg<List<CarPosition>>>(){}.getType();
		return gson.fromJson(line, type);
	};
	
	public static ReceiveMsg<CarId> crash(String line){
		@SuppressWarnings("serial")
		Type type = new TypeToken<ReceiveMsg<CarId>>(){}.getType();
		return gson.fromJson(line, type);
	};
	
	public static ReceiveMsg<CarId> spawn(String line){
		@SuppressWarnings("serial")
		Type type = new TypeToken<ReceiveMsg<CarId>>(){}.getType();
		return gson.fromJson(line, type);
	};
	
	public static ReceiveMsg<GameInit> gameInit(String line){
		@SuppressWarnings("serial")
		Type type = new TypeToken<ReceiveMsg<GameInit>>(){}.getType();
		return gson.fromJson(line, type);
	};
	
	public static ReceiveMsg<CarId> yourCar(String line){
		return crash(line);
	};
	
	public static ReceiveMsg<TurboAvailable> turboAvailable(String line){
		@SuppressWarnings("serial")
		Type type = new TypeToken<ReceiveMsg<TurboAvailable>>(){}.getType();
		return gson.fromJson(line, type);
	};
}
